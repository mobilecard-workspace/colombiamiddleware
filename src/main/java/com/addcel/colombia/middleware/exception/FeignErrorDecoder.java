package com.addcel.colombia.middleware.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import feign.Response;
import feign.codec.ErrorDecoder;

@Component
public class FeignErrorDecoder implements ErrorDecoder {
	
	private static final Logger LOG = LoggerFactory.getLogger(FeignErrorDecoder.class);

	@Override
	public Exception decode(String methodKey, Response response) {
		switch (response.status()) {
		case 400:
			LOG.error("codigo de status: "+response.status()+", metodo: "+methodKey);
		case 404:
			LOG.error("No se encuentra el servicio. Metodo: "+methodKey);
			return new ResponseStatusException(HttpStatus.valueOf(response.status()), "No se encuentra el servicio al que quieres acceder");
		case 500:
			LOG.error("Error interno del servicio: "+response.reason()+", metodo: "+methodKey);
			return new ResponseStatusException(HttpStatus.valueOf(response.status()), "Error interno del servicio "+response.reason());
		default:
			return new Exception(response.reason());
		}
	}

}
