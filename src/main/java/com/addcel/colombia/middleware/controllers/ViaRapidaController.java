package com.addcel.colombia.middleware.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.colombia.middleware.entities.dto.ConsultaTagResponse;
import com.addcel.colombia.middleware.entities.dto.DetalleTagResponse;
import com.addcel.colombia.middleware.entities.dto.RequestRecargaTag;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.SaveTagResponse;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.entities.dto.request.viarapida.SaveTagRequest;
import com.addcel.colombia.middleware.service.ViaRapidaService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@RestController
public class ViaRapidaController {
	
	@Autowired
	private ViaRapidaService service;
	@Autowired
	private BdUtils bdUtils;

	private final static String INVALIDCOUNTRY = "Servicio valido solamente para Colombia";
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseMc> handleException(Exception e) {
        e.printStackTrace();
        int code = -1;
        String message = e.getMessage();
        return new ResponseEntity<>(new ResponseMc(code, message), HttpStatus.OK);
    }
	
	@PostMapping(value = AppUtils.SAVE_TAG)
	public ResponseEntity<SaveTagResponse> saveTag(@RequestBody SaveTagRequest request) throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<SaveTagResponse>(service.saveTag(request), HttpStatus.OK);
	}
	
	@DeleteMapping(value = AppUtils.DELETE_TAG)
	public ResponseEntity<ResponseMc> deleteTag(@RequestBody SaveTagRequest request)throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ResponseMc>(service.deleteTag(request), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.LIST_TAGS)
	public ResponseEntity<ConsultaTagResponse> listTags(@PathVariable("idUsuario") long idUsuario, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ConsultaTagResponse>(service.listTagbyUser(idUsuario), HttpStatus.OK);
	}
	
	@PostMapping(value = AppUtils.RECARGA_TAG)
	public ResponseEntity<TransactionResponse> recargarTag(@RequestBody RequestRecargaTag request) throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<TransactionResponse>(service.recargaTag(request), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.DETALLE_TAG)
	public ResponseEntity<DetalleTagResponse> detalleTag(@PathVariable("idUsuario") long idUsuario, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma, @PathVariable("idTag") Long idTag) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<DetalleTagResponse>(service.detalleTag(idUsuario, idTag, idApp, idPais), HttpStatus.OK);
	}
}
