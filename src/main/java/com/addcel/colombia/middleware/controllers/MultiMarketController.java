package com.addcel.colombia.middleware.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.colombia.middleware.entities.dto.CatalogoMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.FacturaMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.MultiMarketServices;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.ServiciosMultiMarket;
import com.addcel.colombia.middleware.entities.dto.ServiciosMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.entities.dto.request.ConsultaFacturaRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.CompraProductosRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.PagoServicioMmp;
import com.addcel.colombia.middleware.service.MultiMarketService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@RestController
public class MultiMarketController {
	
	@Autowired
	private MultiMarketService service;
	@Autowired
	private BdUtils bdUtils;

	private final static String INVALIDCOUNTRY = "Servicio valido solamente para Colombia";
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseMc> handleException(Exception e) {
        e.printStackTrace();
        int code = -1;
        String message = e.getMessage();
        return new ResponseEntity<>(new ResponseMc(code, message), HttpStatus.OK);
    }
	
	@GetMapping(value = AppUtils.CTR_CONSULTA_PRODUCTOS)
	public ResponseEntity<CatalogoMultiMarketResponse> catalogoProductos(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma) throws Exception{
		if(bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<CatalogoMultiMarketResponse>(service.consultaProductos(idApp, idPais, idioma), HttpStatus.OK);
	}
	
	@PostMapping(value  = AppUtils.CTRL_COMPRA)
	public ResponseEntity<TransactionResponse> comprarProducto(@RequestBody CompraProductosRequest request) throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<TransactionResponse>(service.compraProducto(request), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.CTRL_CONSULTA_SERVICIO)
	public ResponseEntity<List<ServiciosMultiMarket>> consultaServicio(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma, @RequestParam("servicio") String servicio) throws Exception{
		if (bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<List<ServiciosMultiMarket>>(service.buscador(idApp, idPais, idioma, servicio), HttpStatus.OK);
	}
	
	@PostMapping(value = AppUtils.CTRL_PAGO_SERVICIO)
	public ResponseEntity<TransactionResponse> pagoServicio(@RequestBody PagoServicioMmp request) throws Exception{
		if (bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<TransactionResponse>(service.pagoServicio(request), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.CTRL_CATALOGO_SERVICIOS)
	public ResponseEntity<ServiciosMultiMarketResponse> catalogoServicios(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma, @RequestParam("categoria") String categoria) throws Exception{
		if (bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ServiciosMultiMarketResponse>(service.consultaServicios(idApp, idPais, idioma, categoria), HttpStatus.OK);
	}
	
	@GetMapping(value = AppUtils.CTRL_CATALOGO_CATEGORIAS)
	public ResponseEntity<MultiMarketServices> consultarCategorias(@PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma) throws Exception{
		if (bdUtils.validaPais(idPais))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<MultiMarketServices>(service.consultaCategorias(idApp, idPais, idioma), HttpStatus.OK);
	}
	
	@PostMapping(value = AppUtils.CTRL_ESACANEA_FACTURA)
	public ResponseEntity<FacturaMultiMarketResponse> consultarFactura(@RequestBody ConsultaFacturaRequest request) throws Exception{
		return new ResponseEntity<FacturaMultiMarketResponse>(service.consultaFactura(request), HttpStatus.OK);
	}
	
}
