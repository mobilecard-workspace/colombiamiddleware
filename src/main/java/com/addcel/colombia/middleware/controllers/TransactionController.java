package com.addcel.colombia.middleware.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.colombia.middleware.entities.dto.RequestPaqueteService;
import com.addcel.colombia.middleware.entities.dto.RequestRecargaService;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.service.TransactionService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@RestController
public class TransactionController {

	@Autowired
	private TransactionService service;
	@Autowired 
	BdUtils bdUtils;
	
	private final static String INVALIDCOUNTRY = "Servicio valido solamente para Colombia";
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseMc> handleException(Exception e) {
        e.printStackTrace();
        int code = -1;
        String message = e.getMessage();
        return new ResponseEntity<>(new ResponseMc(code, message), HttpStatus.OK);
    }
	
	@PostMapping(value = AppUtils.RECARGAS)
	public ResponseEntity<TransactionResponse> realizarRecarga(@RequestBody RequestRecargaService request) throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<TransactionResponse>(service.recarga(request), HttpStatus.OK);
	}
	
	@PostMapping(value = AppUtils.COMPRA_PAQUETE)
	public ResponseEntity<TransactionResponse> comprarPaquete(@RequestBody RequestPaqueteService request) throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<TransactionResponse>(service.comprarPaquete(request), HttpStatus.OK);
	}
}
