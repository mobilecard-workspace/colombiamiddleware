package com.addcel.colombia.middleware.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.colombia.middleware.entities.dto.CalculaPolizaRequest;
import com.addcel.colombia.middleware.entities.dto.CalcularPolizaResponse;
import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.request.soat.ExpedirPolizaRequest;
import com.addcel.colombia.middleware.service.SoatService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@RestController
public class SoatController {

	@Autowired 
	private BdUtils bdUtils;
	@Autowired
	private SoatService service;
	
	private final static String INVALIDCOUNTRY = "Servicio valido solamente para Colombia";
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseMc> handleException(Exception e) {
        e.printStackTrace();
        int code = -1;
        String message = e.getMessage();
        return new ResponseEntity<>(new ResponseMc(code, message), HttpStatus.OK);
    }
	
	@PostMapping(value = AppUtils.CTR_CALCULA_POLIZA)
	public ResponseEntity<CalcularPolizaResponse> calcularPoliza(@RequestBody CalculaPolizaRequest request)throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<>(service.calcularPoliza(request), HttpStatus.OK);
	}
	
	@PostMapping(value = AppUtils.CTR_EXPEDIR_POLIZA)
	public ResponseEntity<ResponseMc> expedirPoliza(@RequestBody ExpedirPolizaRequest request) throws Exception{
		if(bdUtils.validaPais(request.getIdPais()))
			throw new Exception(INVALIDCOUNTRY);
		else
			return new ResponseEntity<ResponseMc>(service.expedirPoliza(request), HttpStatus.OK);
	}
}
