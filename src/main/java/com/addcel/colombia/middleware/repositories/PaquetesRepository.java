package com.addcel.colombia.middleware.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.Paquetes;

@Repository
public interface PaquetesRepository extends JpaRepository<Paquetes, Integer> {
	List<Paquetes> findByCategoria_CodigoCategoria(String codigoCategoria);
}
