package com.addcel.colombia.middleware.repositories.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.addcel.colombia.middleware.entities.dto.request.viarapida.ConsultaSaldoRequest;
import com.addcel.colombia.middleware.entities.dto.request.viarapida.ConsultarPasos;
import com.addcel.colombia.middleware.entities.dto.request.viarapida.RecargarTagRequest;
import com.addcel.colombia.middleware.entities.dto.response.viarapida.BaseViaRapidaResponse;
import com.addcel.colombia.middleware.entities.dto.response.viarapida.SaldoTagResponse;
import com.addcel.colombia.middleware.utils.AppUtils;

@FeignClient(name = "viaRapida", url =AppUtils.BASEURI_VIARAPIDA)
public interface ViaRapidaClient {

	@PostMapping(value = AppUtils.CONSULTASALDO_VIA)
	SaldoTagResponse consultarSaldo(@RequestBody ConsultaSaldoRequest request, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais);
	
	@PostMapping(value = AppUtils.RECARGATAG_VIA)
	BaseViaRapidaResponse recargaTag(@RequestBody RecargarTagRequest request, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais);
	
	@PostMapping(value = AppUtils.CONSULTARPASOS_VIA)
	BaseViaRapidaResponse consultarPasos(@RequestBody ConsultarPasos request, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais);
}
