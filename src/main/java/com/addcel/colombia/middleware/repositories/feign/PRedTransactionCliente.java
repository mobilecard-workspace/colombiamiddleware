package com.addcel.colombia.middleware.repositories.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import com.addcel.colombia.middleware.entities.dto.request.BaseRequest;
import com.addcel.colombia.middleware.entities.dto.request.BaseRequestPaquete;
import com.addcel.colombia.middleware.entities.dto.response.BaseResponse;
import com.addcel.colombia.middleware.repositories.feign.config.FeignClientConfiguration;
import com.addcel.colombia.middleware.utils.AppUtils;

@FeignClient(name = "puntoRedCliente", url = AppUtils.BASEURI_PUNTORED_TRANSACTION, configuration = FeignClientConfiguration.class)
public interface PRedTransactionCliente {

	@PostMapping(value = AppUtils.BASEURI_PUNTORED_RECARGA)
	BaseResponse realizarRecarga(BaseRequest request);
	
	@PostMapping(value = AppUtils.BASEURI_PUNTORED_PAQUETE)
	BaseResponse compradePaquete(BaseRequestPaquete request);
}
