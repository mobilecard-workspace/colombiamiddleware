package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.addcel.colombia.middleware.entities.model.Paises;

public interface PaisesRepository extends JpaRepository<Paises, Integer> {

}
