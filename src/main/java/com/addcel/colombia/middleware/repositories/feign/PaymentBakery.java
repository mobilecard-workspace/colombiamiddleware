package com.addcel.colombia.middleware.repositories.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.addcel.colombia.middleware.entities.dto.request.PaymentBRequest;
import com.addcel.colombia.middleware.entities.dto.response.PaymentBResponse;
import com.addcel.colombia.middleware.repositories.feign.config.FeignClientConfiguration;
import com.addcel.colombia.middleware.utils.AppUtils;

@FeignClient(name = "paymentBakery", url = AppUtils.URI_BASE_PAYMENT_BAKERY, configuration = FeignClientConfiguration.class)
public interface PaymentBakery {

	@PostMapping(value = AppUtils.URI_PAYMENT_BAKERY)
	public PaymentBResponse procesaPago(@RequestBody PaymentBRequest request, @PathVariable("idApp") Long idApp, @PathVariable("idPais") Integer idPais, @PathVariable("idioma") String idioma);
	
}
