package com.addcel.colombia.middleware.repositories.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.addcel.colombia.middleware.entities.dto.request.push.PushRequest;
import com.addcel.colombia.middleware.entities.dto.response.push.PushResponse;
import com.addcel.colombia.middleware.utils.AppUtils;

@FeignClient(name = "push", url = AppUtils.PUSH_NOTIFICATION)
public interface PushClient {

	@PostMapping(value = AppUtils.SEND_PUSH)
	PushResponse sendPushNotification(@RequestBody PushRequest request);
}
