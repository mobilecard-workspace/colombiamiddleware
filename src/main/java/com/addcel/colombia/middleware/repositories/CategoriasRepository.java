package com.addcel.colombia.middleware.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.Categorias;

@Repository
public interface CategoriasRepository extends JpaRepository<Categorias, Integer> {
	List<Categorias> findByProveedor_CodigoProveedor(String codigoProveedor);
	Categorias findByCodigoCategoria(String codigoCategoria);
}
