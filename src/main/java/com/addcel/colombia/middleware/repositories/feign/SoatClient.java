package com.addcel.colombia.middleware.repositories.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.request.soat.ConsultaPlacaRequest;
import com.addcel.colombia.middleware.entities.dto.request.soat.ConsultaPolizaRequest;
import com.addcel.colombia.middleware.entities.dto.request.soat.RequestExpedirPoliza;
import com.addcel.colombia.middleware.entities.dto.response.soat.CalculaPolizaResponse;
import com.addcel.colombia.middleware.entities.dto.response.soat.ConsultarInfoVehiculoResponse;
import com.addcel.colombia.middleware.utils.AppUtils;

@FeignClient(name = "soat", url =AppUtils.BASEURI_SOAT)
public interface SoatClient {
	
	@PostMapping(value = AppUtils.CALCULA_POLIZA)
	CalculaPolizaResponse calcularPoliza(ConsultaPolizaRequest request);
	
	@PostMapping(value = AppUtils.CONSULTA_PLACA)
	ConsultarInfoVehiculoResponse consultaInfoVehiculo(ConsultaPlacaRequest request);
	
	@PostMapping(value = AppUtils.EXPEDIR_POLIZA)
	ResponseMc expedirPoliza(RequestExpedirPoliza request);
}
