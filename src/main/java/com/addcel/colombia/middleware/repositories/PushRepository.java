package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.PushNotification;

@Repository
public interface PushRepository extends JpaRepository<PushNotification, Long> {
	
	@Query("SELECT p.id FROM PushNotification p WHERE p.modulo = :modulo AND p.idioma = :idioma")
	Long findByModuloAndIdioma(@Param("modulo") String modulo, @Param("idioma") String idioma);
}
