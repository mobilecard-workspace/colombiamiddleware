package com.addcel.colombia.middleware.repositories.feign.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.addcel.colombia.middleware.entities.dto.AppUser;

import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import feign.slf4j.Slf4jLogger;

@Configuration
public class FeignClientConfiguration {
	
	@Autowired
	private AppUser appUser;

	@Bean
	Logger feignLoggerLevel(){
		return new Slf4jLogger();
	}
	
	@Bean
	public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
		return new BasicAuthRequestInterceptor(appUser.getUsername(), appUser.getPassword());
	}
}
