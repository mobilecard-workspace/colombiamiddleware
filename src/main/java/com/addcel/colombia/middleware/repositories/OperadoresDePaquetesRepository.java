package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.addcel.colombia.middleware.entities.model.OperadoresDePaquetes;

@Repository
public interface OperadoresDePaquetesRepository extends JpaRepository<OperadoresDePaquetes, Integer> {
	OperadoresDePaquetes findByCodigoProveedor(String codigoProveedor);
}
