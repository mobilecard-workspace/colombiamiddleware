package com.addcel.colombia.middleware.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.addcel.colombia.middleware.entities.model.TablaParametrica;

public interface TablaParametricaRepository extends JpaRepository<TablaParametrica, Long> {

	@Query("SELECT COUNT(t) FROM TablaParametrica t WHERE t.organismo = ?1 AND t.estado = ?2")
	Integer validateOrganismo(String organismo, String estado); 
}
