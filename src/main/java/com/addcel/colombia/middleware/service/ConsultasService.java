package com.addcel.colombia.middleware.service;

import com.addcel.colombia.middleware.entities.dto.ConsultaSaldoResponse;
import com.addcel.colombia.middleware.entities.dto.response.EstadoTransaccionResponse;

public interface ConsultasService {

	ConsultaSaldoResponse consultarSaldoPuntoVenta(long idEstablecimiento, long idApp, Integer idPais, String idioma) throws Exception;
	EstadoTransaccionResponse consultaStatusTransaccion(long idEstablecimiento, String trace, long idApp, Integer idPais, String idioma) throws Exception;
	
}
