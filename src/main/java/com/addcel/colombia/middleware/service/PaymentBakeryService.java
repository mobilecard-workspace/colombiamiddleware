package com.addcel.colombia.middleware.service;

import com.addcel.colombia.middleware.entities.dto.PaymentBakeryDTO;
import com.addcel.colombia.middleware.entities.dto.response.PaymentBResponse;

public interface PaymentBakeryService {

	public PaymentBResponse cobrarMontoPaymentBakery(PaymentBakeryDTO dto) throws Exception;
	
}
