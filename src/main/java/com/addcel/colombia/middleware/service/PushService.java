package com.addcel.colombia.middleware.service;

import java.util.Map;

public interface PushService {

	void sendPush(Map<String, String> params, String idioma, Integer idPais, Integer idApp, long idUsuario, String modulo);
}
