package com.addcel.colombia.middleware.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.PaymentBakeryDTO;
import com.addcel.colombia.middleware.entities.dto.RequestPaqueteService;
import com.addcel.colombia.middleware.entities.dto.RequestRecargaService;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.entities.dto.request.BaseRequest;
import com.addcel.colombia.middleware.entities.dto.request.BaseRequestPaquete;
import com.addcel.colombia.middleware.entities.dto.request.PaqueteRequest;
import com.addcel.colombia.middleware.entities.dto.request.RecargaRequest;
import com.addcel.colombia.middleware.entities.dto.response.BaseResponse;
import com.addcel.colombia.middleware.entities.dto.response.PaymentBResponse;
import com.addcel.colombia.middleware.entities.model.ConfigPuntoRed;
import com.addcel.colombia.middleware.entities.model.Paquetes;
import com.addcel.colombia.middleware.repositories.feign.PRedTransactionCliente;
import com.addcel.colombia.middleware.service.PaymentBakeryService;
import com.addcel.colombia.middleware.service.PushService;
import com.addcel.colombia.middleware.service.TransactionService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@Service
public class TransactionServiceImpl implements TransactionService {
	
	private static final Logger LOG = LoggerFactory.getLogger(TransactionServiceImpl.class); 

	@Autowired
	private PaymentBakeryService paymentService;
	@Autowired
	private PRedTransactionCliente pRedCliente;
	@Autowired
	private BdUtils bdUtils;
	@Autowired
	private PushService pushService;
	
	
	@Override
	public TransactionResponse recarga(RequestRecargaService request) throws Exception {
		TransactionResponse response = null;
		BaseRequest requestService = null;
		BaseResponse responsePr = null;
		ConfigPuntoRed configPr = null;
		PaymentBResponse paymentResponse = null;
		try {
			LOG.info("Inicializando servicio de recarga: {}",request);
			LOG.info("Validando al usuario y la tarjeta en la Base de Datos");
			Integer montoString = Double.valueOf(request.getMonto()).intValue();
			if(bdUtils.validarUsuario(request.getIdUsuario()) && bdUtils.obtenerDatosTarjeta(request.getIdTarjeta())) {
				paymentResponse = paymentService.cobrarMontoPaymentBakery(new PaymentBakeryDTO(request.getIdUsuario(), request.getIdTarjeta(), Integer.toString(montoString*100), request.getImei(), request.getConcepto(), request.getIdApp(), request.getIdPais(), request.getIdioma(), 210L, 2100L, "0.00"));
				if (paymentResponse.getCode() != -3100 && !paymentResponse.getOrder().getTransactions().get(0).getStatus().equals("ERROR")){
					requestService = new BaseRequest();
					LOG.info("Obteniendo las credenciales de Punto Red");
					configPr = bdUtils.generateRequestPuntoRed();
					LOG.info("Credenciales Punto Red: {}",configPr.getPuntoVenta());
					requestService.setClaveHost(configPr.getClaveHost());
					requestService.setComercio(configPr.getComercio());
					requestService.setProceso(AppUtils.COD_PROCESO_RECARGA);
					requestService.setPuntoVenta(configPr.getPuntoVenta());
					requestService.setUsuarioHost(configPr.getUsuarioHost());
					LOG.info("Generando la peticion de Punto Red");
					
					requestService.setDatos(new RecargaRequest(request.getNumeroCelular(), Integer.toString(montoString) , configPr.getTerminal(), configPr.getClaveCxr(), request.getCodigoProveedor(), paymentResponse.getOrder().getTransactions().get(0).getAuthorization()));
					LOG.info("Enviando la peticion");
					responsePr = pRedCliente.realizarRecarga(requestService);
					LOG.info("Respuesta del servicio: {}",responsePr);
					if (responsePr.getCodigo().equals("00")) 
						response = new TransactionResponse(0, "Recarga exitosa", responsePr.getDatos().getIdTransaccion());
					else
						throw new Exception("Mensaje Punto Red: "+responsePr.getMensaje()+", codigo de error: "+responsePr.getCodigo());
					
					bdUtils.ingresarBitacoraPuntoRed("Recarga Punto Red", "Recarga exitosa", request.getIdUsuario(), "USUARIO", "Punto Red Transaccion", request.getIdPais(), request.getIdApp(), request.getIdioma(), 210L, 2100L);
					Map<String, String> params = new HashMap<String, String>();
					params.put("<proveedor>", bdUtils.nombreOperadorRecarga(request.getCodigoProveedor()));
					params.put("<monto>",Integer.toString(montoString));
					pushService.sendPush(params, request.getIdioma(), request.getIdPais(), request.getIdApp().intValue(), request.getIdUsuario(), "recarga_pr_exito");
				}else {
					if (paymentResponse.getCode() == -3100) {
						throw new Exception(paymentResponse.getMessage());
					}else {
						throw new Exception("Transaccion Denegada por Payment Bakery");
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error al realizar una recarga {}",e.getMessage());
			bdUtils.ingresarBitacoraPuntoRed("Recarga Punto Red al proveedor: "+request.getCodigoProveedor(), "Error al realizar la recarga "+e.getMessage(), request.getIdUsuario(), "USUARIO", "Punto Red Transaccion", request.getIdPais(), request.getIdApp(), request.getIdioma(), 210L, 2100L);
			throw new Exception("Error al realizar recarga: "+e.getMessage());
		}
		return response;
	}

	@Override
	public TransactionResponse comprarPaquete(RequestPaqueteService request) throws Exception{
		TransactionResponse response = null;
		ConfigPuntoRed config = null;
		PaymentBResponse paymentResponse = null;
		BaseRequestPaquete requestService = null;
		BaseResponse responseService = null;
		Paquetes paquete = null;
		try {
			LOG.info("Inicializando servicio compra de paquete: {}",request);
			LOG.info("Validando al usuario y la tarjeta en la Base de Datos");
			if(bdUtils.validarUsuario(request.getIdUsuario()) && bdUtils.obtenerDatosTarjeta(request.getIdTarjeta())) {
				LOG.info("Usuario y tajetas válidos");
				LOG.info("Consultando el paquete");
				paquete = bdUtils.consultaPaquete(request.getIdPaquete());
				String montoString = paquete.getValor();
				Integer montoInt = (Integer.parseInt(montoString))*100;
				paymentResponse = paymentService.cobrarMontoPaymentBakery(new PaymentBakeryDTO(request.getIdUsuario(), request.getIdTarjeta(), Integer.toString(montoInt), request.getImei(), "Compra paquete Punto Red "+paquete.getDescripcion(), request.getIdApp(), request.getIdPais(), request.getIdioma(), 210L, 2100L, "0.00"));
				if (paymentResponse.getCode() != -3100 && !paymentResponse.getOrder().getTransactions().get(0).getStatus().equals("ERROR")) {
					requestService = new BaseRequestPaquete();
					LOG.info("Obteniendo las credenciales de Punto Red");
					config = bdUtils.generateRequestPuntoRed();
					LOG.info("Credenciales Punto Red: {}",config.getPuntoVenta());
					requestService.setClaveHost(config.getClaveHost());
					requestService.setComercio(config.getComercio());
					requestService.setProceso(AppUtils.COD_PROCESO_COMPRA_PAQUETE);
					requestService.setPuntoVenta(config.getPuntoVenta());
					requestService.setUsuarioHost(config.getUsuarioHost());
					LOG.info("Enviando la peticion a Punto Red");
					requestService.setDatos(new PaqueteRequest(request.getNumeroCelular(), montoString, config.getTerminal(), config.getClaveCxr(), paquete.getCategoria().getProveedor().getCodigoProveedor(), paymentResponse.getOrder().getTransactions().get(0).getAuthorization(), paquete.getCategoria().getCodigoCategoria(), paquete.getCodigoPaquete(), paquete.getSku()));
					responseService = pRedCliente.compradePaquete(requestService);
					if(responseService.getCodigo().equals("00"))
						response = new TransactionResponse(0, "Compra exitosa", responseService.getDatos().getIdTransaccion());
					else
						throw new Exception("Mensaje Punto Red: "+responseService.getMensaje()+", codigo de error: "+responseService.getCodigo());
					
					bdUtils.ingresarBitacoraPuntoRed("Compra de Paquete: "+paquete.getDescripcion()+", valor: "+montoString, "Compra exitosa", request.getIdUsuario(), "USUARIO", "Punto Red Transaccion", request.getIdPais(), request.getIdApp(), request.getIdioma(), 210L, 2100L);
					Map<String, String> params = new HashMap<String, String>();
					params.put("<proveedor>", paquete.getDescripcion());
					pushService.sendPush(params, request.getIdioma(), request.getIdPais(), request.getIdApp().intValue(), request.getIdUsuario(), "compra_paquete_exitoso");
				}else {
					if (paymentResponse.getCode() == -3100) {
						throw new Exception(paymentResponse.getMessage());
					}else {
						throw new Exception("Transaccion Denegada por Payment Bakery");
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error al comprar un paquete "+e.getMessage());
			bdUtils.ingresarBitacoraPuntoRed("Compra de Paquete: "+paquete.getDescripcion()+", valor: "+paquete.getValor(), "Error al realizar la compra "+e.getMessage(), request.getIdUsuario(), "USUARIO", "Punto Red Transaccion", request.getIdPais(), request.getIdApp(), request.getIdioma(), 210L, 2100L);
			throw new Exception("Error al comprar un paquete "+e.getMessage());
		}
		return response;
	}

}
