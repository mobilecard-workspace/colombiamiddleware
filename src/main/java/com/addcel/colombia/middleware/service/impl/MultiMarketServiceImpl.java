package com.addcel.colombia.middleware.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.CatalogoMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.CategoriaMmService;
import com.addcel.colombia.middleware.entities.dto.FacturaMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.MultiMarketProductoObj;
import com.addcel.colombia.middleware.entities.dto.MultiMarketServices;
import com.addcel.colombia.middleware.entities.dto.PaymentBakeryDTO;
import com.addcel.colombia.middleware.entities.dto.ServiciosMultiMarket;
import com.addcel.colombia.middleware.entities.dto.ServiciosMultiMarketResponse;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;
import com.addcel.colombia.middleware.entities.dto.ValoresMultiMarket;
import com.addcel.colombia.middleware.entities.dto.request.ConsultaFacturaRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.CompraProductosRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.PagoServicioMmp;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.ProductQuery;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.ProductSellAmount;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.ProductsSellRequest;
import com.addcel.colombia.middleware.entities.dto.request.multimarket.ServicePayRequest;
import com.addcel.colombia.middleware.entities.dto.response.PaymentBResponse;
import com.addcel.colombia.middleware.entities.dto.response.multimarket.BillDataResponse;
import com.addcel.colombia.middleware.entities.dto.response.multimarket.BillSearchListResponse;
import com.addcel.colombia.middleware.entities.dto.response.multimarket.ProductsSellResponse;
import com.addcel.colombia.middleware.entities.model.ConveniosMultiMarket;
import com.addcel.colombia.middleware.entities.model.FacturaMultiMarket;
import com.addcel.colombia.middleware.entities.model.MultiMarketProductos;
import com.addcel.colombia.middleware.entities.model.Usuario;
import com.addcel.colombia.middleware.repositories.ConveniosMultimarketRepository;
import com.addcel.colombia.middleware.repositories.FacturaRepository;
import com.addcel.colombia.middleware.repositories.MultiMarketRepository;
import com.addcel.colombia.middleware.repositories.feign.MultiMarketClient;
import com.addcel.colombia.middleware.service.MultiMarketService;
import com.addcel.colombia.middleware.service.PaymentBakeryService;
import com.addcel.colombia.middleware.service.PushService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@Service
public class MultiMarketServiceImpl implements MultiMarketService {

	@Autowired
	private MultiMarketRepository repository;
	@Autowired
	private ConveniosMultimarketRepository conveniosrepo;
	@Autowired
	private BdUtils bdUtils;
	@Autowired
	private PaymentBakeryService paymentService;
	@Autowired
	private MultiMarketClient marketClient;
	@Autowired
	private PushService pushService;
	@Autowired
	private FacturaRepository facturaRepo;
	
	private static final Logger LOG = LoggerFactory.getLogger(MultiMarketServiceImpl.class);
	
	@Override
	public CatalogoMultiMarketResponse consultaProductos(Long idApp, Integer idPais, String idioma) throws Exception {
		CatalogoMultiMarketResponse response = null;
		List<MultiMarketProductoObj> objResponse = new ArrayList<MultiMarketProductoObj>();;
		List<MultiMarketProductos> categorias = null;
		LOG.info("Inicializando servicio de consulta de productos");
		try {
			categorias = repository.findAllGroupByName();
			if(categorias != null) {
				List<MultiMarketProductos> producto = repository.findAll();
				response = new CatalogoMultiMarketResponse();
				response.setCodigo(0);
				response.setMensaje("Consulta Exitosa");
				categorias.forEach(c -> {
					MultiMarketProductoObj obj = new MultiMarketProductoObj();
					obj.setNombre(c.getNombre());
					List<ValoresMultiMarket> valores = new ArrayList<ValoresMultiMarket>();
					producto.forEach(p -> {
						if (obj.getNombre().equals(p.getNombre())) {
							ValoresMultiMarket valor = new ValoresMultiMarket(p.getIdProducto().intValue(), p.getValor());
							valores.add(valor);
						}
					});
					obj.setValor(valores);
					objResponse.add(obj);
				});
				response.setProductos(objResponse);
			}else {
				LOG.info("No hay productos registrados en la BD");
				throw new Exception("No hay productos registrados en la BD");
			}
		} catch (Exception e) {
			LOG.error("Error al consultar el catalogo de productos, {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}

	@Override
	public TransactionResponse compraProducto(CompraProductosRequest request) throws Exception {
		TransactionResponse response = null;
		MultiMarketProductos producto = null;
		Usuario user = null;
		//PaymentBResponse paymentResponse = null;
		ProductsSellResponse multiResponse = null;
		try {
			LOG.info("Inicializando el servicio de compra de productos, request:{}",request);
			if (bdUtils.validarUsuario(request.getIdUsuario()) && bdUtils.obtenerDatosTarjeta(request.getIdTarjeta()) != null) {
				LOG.info("Obteniendo los datos del producto de la BD");
				producto = repository.findById(request.getIdProducto()).orElse(null);
				if(producto != null){
					user = bdUtils.consultarUsuario(request.getIdUsuario());
					//paymentResponse = paymentService.cobrarMontoPaymentBakery(new PaymentBakeryDTO(request.getIdUsuario(), request.getIdTarjeta(), request.getAmount()!=0.00?request.getAmount():new Double(producto.getValor()), "", "Pago PIN "+producto.getNombre(), request.getIdApp(), request.getIdPais(), request.getIdioma(), 210L, 2100L, request.getComision()));
					//if (paymentResponse.getCode() != -3100 && !paymentResponse.getOrder().getTransactions().get(0).getStatus().equals("ERROR")) {
						multiResponse = compraMultiRequest(producto, user, request.getAmount());
						response = crearRespuesta(multiResponse, "", producto.getNombre(), "pago_pin", producto.getNombre().equals("RECARGA WPLAY")?request.getAmount().toString():producto.getValor(), request.getIdPais(), request.getIdApp(), request.getIdUsuario(), request.getIdioma(), user.getNombre());
					/*} else {
						if (paymentResponse.getCode() == -3100) {
							throw new Exception(paymentResponse.getMessage());
						}else {
							throw new Exception("Transaccion Denegada por Payment Bakery");
						}				
					}*/
				}else{
					LOG.error("El producto con id {} no existe en la BD", request.getIdProducto());
					throw new Exception("El producto con id "+request.getIdProducto()+" no existe en la BD");
				}
			} else {
				LOG.error("El usuario o la tarjeta no existen en la BD");
				throw new Exception("Usuario o tarjeta inválidos");
			}
		} catch (Exception e) {
			LOG.error("Error al comprar un producto, {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	@Override
	public List<ServiciosMultiMarket> buscador(Long idApp, Integer idPais, String idioma, String palabra) throws Exception {
		List<ServiciosMultiMarket> response = new ArrayList<ServiciosMultiMarket>();
		List<ConveniosMultiMarket> servicios = null;
		try {
			LOG.info("Iniciando la busqueda del servicio: {}", palabra);
			servicios = conveniosrepo.findByNombreContaining(palabra);
			LOG.info("Resultados encontrados: {}",servicios);
			servicios.forEach(s -> {
				ServiciosMultiMarket c = new ServiciosMultiMarket(s.getIdConvenio().intValue(), s.getNombre());
				response.add(c);
			});
		} catch (Exception e) {
			LOG.error("Error al buscar el servicio en la BD: {}", e.getMessage());
			throw new Exception("Error al buscar el servicio en la BD: "+e.getMessage());
		}
		return response;
	}
	
	@Override
	public MultiMarketServices consultaCategorias(Long idApp, Integer idPais, String idioma) throws Exception {
		MultiMarketServices response = null;
		List<CategoriaMmService> categoriasResponse = new ArrayList<CategoriaMmService>();
		try {
			List<String> categorias = conveniosrepo.findCategorias();
			categorias.forEach(c -> {
				CategoriaMmService cat = new CategoriaMmService();
				cat.setNombre(c);
				categoriasResponse.add(cat);
			});
			response = new MultiMarketServices(categoriasResponse);
			response.setCodigo(0);
			response.setMensaje("Consulta exitosa");
		} catch (Exception e) {
			LOG.info("Error al consultar los servicios {}",e.getMessage());
		}
		return response;
	}

	@Override
	public TransactionResponse pagoServicio(PagoServicioMmp request) throws Exception {
		TransactionResponse response = null;
		ServicePayRequest payRequest = null;
		Usuario user = null;
		ProductsSellResponse mmpResponse = null;
		PaymentBResponse paymentResponse = null;
		FacturaMultiMarket billData = null;
		LOG.info("Realizando pago de servicio Multimarket Place, request:{}", request);
		try {
			if (bdUtils.validarUsuario(request.getIdUsuario()) && bdUtils.obtenerDatosTarjeta(request.getIdTarjeta()) != null) {
				LOG.info("Consultando la información del usuario");
				user = bdUtils.consultarUsuario(request.getIdUsuario());
				billData = facturaRepo.findById(request.getIdReferencia()).orElse(null);
				if (billData != null) {
					Integer monto = billData.getAmountEditable()?request.getMonto().intValue():billData.getMonto().intValue();
					Integer comision = billData.getAmountEditable()?request.getComision().intValue():billData.getComision().intValue();
					paymentResponse = paymentService.cobrarMontoPaymentBakery(new PaymentBakeryDTO(
							request.getIdUsuario(), request.getIdTarjeta(), Integer.toString(monto*100)
							, "", "Pago Servicio " + billData.getServicio(),
							request.getIdApp(), request.getIdPais(), request.getIdioma(), 210L, 2100L, Integer.toString(comision*100)));
					if (paymentResponse.getCode() != -3100 && !paymentResponse.getOrder().getTransactions().get(0).getStatus().equals("ERROR")) {
						Map<String, String> data = new HashMap<String, String>();
						data.put(billData.getBarcode() ? "barcode" : "reference", billData.getReference());
						data.put(AppUtils.MMP_CELLPHONE, user.getTelefono());
						payRequest = new ServicePayRequest(billData.getProductId(), billData.getAmountEditable()?request.getMonto().intValue():billData.getMonto().intValue(), billData.getHash(), data);
						LOG.info("Request MultiMarket Place: {}", payRequest);
						mmpResponse = marketClient.pagarServicio(payRequest);
						String cobro = billData.getAmountEditable()?request.getMonto().toString()+request.getComision().toString():billData.getMonto().toString()+billData.getComision().toString();
						response = crearRespuesta(mmpResponse, billData.getServicio(), "", "pago_servicio",
								cobro, request.getIdPais(),
								request.getIdApp(), request.getIdUsuario(), request.getIdioma(), user.getNombre());
					} else {
						if (paymentResponse.getCode() == -3100) {
							throw new Exception(paymentResponse.getMessage());
						}else {
							throw new Exception("Transaccion Denegada por Payment Bakery");
						}
					}
				}else {
					LOG.error("No existen datos para esa factura en la BD");
					throw new Exception("No existe facturas con el id: "+request.getIdReferencia());
				}
			}else {
				LOG.error("El usuario o la tarjeta no existen en la BD");
				throw new Exception("Usuario o tarjeta inválidos");
			}
		} catch (Exception e) {
			LOG.error("Error al pagar el servicio: {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	@Override
	public ServiciosMultiMarketResponse consultaServicios(Long idApp, Integer idPais, String idioma, String categoria) throws Exception {
		ServiciosMultiMarketResponse response = null;
		List<ConveniosMultiMarket> convenios = null;
		try {
			LOG.info("Consultando los servicios afiliados de la categoria: {}",categoria);
			convenios = conveniosrepo.findByCategoria(categoria);
			if (convenios != null) {
				List<ServiciosMultiMarket> servicios = new ArrayList<ServiciosMultiMarket>();
				convenios.forEach(c -> {
					ServiciosMultiMarket s = new ServiciosMultiMarket(c.getIdConvenio().intValue(), c.getNombre());
					servicios.add(s);
				});
				response = new ServiciosMultiMarketResponse(servicios.size()>30?true:false, servicios);
				response.setCodigo(0);
				response.setMensaje("Consulta exitosa");
			} else {
				LOG.error("No existen convenios para la categoria: {}", categoria);
				throw new Exception("No existen servicios para la categoria: "+categoria);
			}
		} catch (Exception e) {
			LOG.error("Error al consultar los servicios de la categoria {}: {}", categoria, e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	private ProductsSellResponse compraMultiRequest(MultiMarketProductos producto, Usuario user, Double amount) throws Exception {
		ProductsSellResponse response = null;
		try {
			LOG.info("Iniciando la compra del pin");
			if (producto.getAmount()) {
				LOG.info("RECARGA WPLAY");
				ProductSellAmount request = new ProductSellAmount();
				request.setAmount(amount.intValue());
				request.setProductId(Integer.toString(producto.getIdMmp()));
				Map<String, String> data = setParams(producto, user);
				request.setData(data);
				LOG.info("Peticion MultiMarket: {}",request);
				response = marketClient.comprarProductoAmount(request);
				LOG.info("Respuesta MultiMarket {}",response);
			} else {
				LOG.info("Pin: {}",producto.getNombre());
				ProductsSellRequest request = new ProductsSellRequest();
				request.setProductId(Integer.toString(producto.getIdMmp()));
				Map<String, String> data = setParams(producto, user);
				request.setData(data);
				LOG.info("Peticion MultiMarket: {}",request);
				response = marketClient.comprarProducto(request);
				LOG.info("Respuesta MultiMarket {}",response);
			}
		} catch (Exception e) {
			LOG.error("Error al comprar un producto, {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	private Map<String, String> setParams(MultiMarketProductos producto, Usuario user) throws Exception{
		Map<String, String> data = new HashMap<String, String>();
		LOG.info("Seteando los parametros para la peticion");
		try {
			String escape = Pattern.quote(",");
			String[] paramsName = producto.getParams().split(escape);
			LOG.info("Parametros Encontrados: {}",paramsName.length);
			for (String key : paramsName) {
				switch (key) {
				case AppUtils.MMP_DOCUMENT:
					data.put(key, user.getCedula());
					break;
				case AppUtils.MMP_CELLPHONE:
					data.put(key, user.getTelefono());
					break;
				case AppUtils.MMP_OFFICE:
					data.put(key, user.getCurp());
					break;
				case AppUtils.MMP_EMAIL:
					data.put(key, user.getEmail());
					break;
				case AppUtils.MMP_LICENSE:
					data.put(key, user.getCedula());
					break;
				default:
					break;
				}
			}
			LOG.info("Parametros seteados: {}", data.size());
		} catch (Exception e) {
			LOG.error("Error al setear un parametro {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return data;
	}
	
	private BillDataResponse consultarBillDataResponse(Integer productId, Boolean barcode, String reference) throws Exception {
		ProductQuery query = null;
		Map<String, String> data = null;
		BillDataResponse response = null;
		try {
			LOG.info("Iniciando la consulta de la referencia en MultiMarketPlace");
			data = new HashMap<String, String>();
			LOG.info(!barcode?"Pago por referencia":"Pago por codigo de barras");
			data.put(!barcode?"reference":"barcode", reference);
			query = new ProductQuery(productId.toString(), AppUtils.QUERY_HASH, data);
			LOG.info("Request MultiMarket {}", query);
			response = marketClient.consultarFactura(query);
			LOG.info("Respuesta de MultiMarket: {}", response);
		} catch (Exception e) {
			LOG.error("Error al generar la referencia: {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	private BillSearchListResponse consultarServicioEnMultimarket(String servicio) throws Exception{
		List<BillSearchListResponse> respuestaMulti = null;
		BillSearchListResponse response = null;
		ProductQuery query = null;
		Map<String, String> data = null;
		try {
			LOG.info("Buscando el servicio en MultiMarket: {}",servicio);
			data = new HashMap<String, String>();
			data.put("search", servicio);
			query = new ProductQuery("60", AppUtils.QUERY_CONSULTA_SERVICIOS, data);
			LOG.info("Request MultiMarket: {}", query);
			respuestaMulti = marketClient.consultarServicio(query);
			LOG.info("Respuesta de MultiMarket: {}", respuestaMulti);
			response = new BillSearchListResponse(respuestaMulti.get(0).getProductId(), respuestaMulti.get(0).getName(), respuestaMulti.get(0).getImage(), respuestaMulti.get(0).getKey(), respuestaMulti.get(0).getCategory());
		} catch (Exception e) {
			LOG.error("Error al buscar el servicio en MultiMarket: {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	private TransactionResponse crearRespuesta(ProductsSellResponse multiResponse, String servicio, String pin, String modulo, String valor, Integer idPais, Long idApp, Long idUsuario, String idioma, String nombreUsuario) throws Exception{
		TransactionResponse response = null;
		try {
			if (multiResponse.getMessage().contains("Venta exitosa")) {
				response = new TransactionResponse(0, multiResponse.getMessage(), multiResponse.getData().getId().toString());
				Map<String, String> params = new HashMap<String, String>();
				params.put("<nombre>", nombreUsuario);
				params.put(!pin.equals("")?"<pin>":"<servicio>", !pin.equals("")?pin:servicio);
				params.put("<valor>", valor);
				pushService.sendPush(params, idioma, idPais, idApp.intValue(), idUsuario, modulo);
				String accion =!pin.equals("")?"Compra de pin: "+pin:"Pago de servicio: "+servicio; 
				bdUtils.ingresarBitacoraPuntoRed(accion+", valor: "+valor, "Compra exitosa", idUsuario, "USUARIO", "MultiMarket Place", idPais, idApp, idioma, 240L, 2400L);
			}else {
				throw new Exception("Error al comprar el producto en MultiMarket Place");
			}
			
		} catch (Exception e) {
			LOG.info("Error en MultiMarket");
			throw new Exception(e.getMessage());
		}
		return response;
	}

	@Override
	public FacturaMultiMarketResponse consultaFactura(ConsultaFacturaRequest request) throws Exception {
		FacturaMultiMarketResponse response = null;
		BillDataResponse billData = null;
		ConveniosMultiMarket convenio = null;
		BillSearchListResponse serviceResponse = null;
		FacturaMultiMarket factura = null;
		try {
			convenio = conveniosrepo.findById(request.getIdConvenio()).orElse(null);
			if (convenio != null) {
				serviceResponse = consultarServicioEnMultimarket(convenio.getNombre());
				if (serviceResponse.getName().equals(convenio.getNombre())) {
					billData = consultarBillDataResponse(serviceResponse.getProductId(), request.getBarcode(), request.getReference());
					if(!billData.getAmountEditable() && billData.getAmount() == 0) {
						LOG.error("Factura no válida");
						throw new Exception("Factura no válida");
					}else if (billData.getProductId() != null) {
						Double comision = billData.getAmount()*0.05;
						factura = new FacturaMultiMarket();
						factura.setBarcode(request.getBarcode());
						factura.setComision(comision);
						factura.setHash(billData.getHash());
						factura.setIdConvenio(request.getIdConvenio());
						factura.setIdUsuario(request.getIdUsuario());
						factura.setMonto(Double.valueOf(billData.getAmount()));
						factura.setProductId(billData.getProductId());
						factura.setReference(request.getReference());
						factura.setServicio(convenio.getNombre());
						factura.setAmountEditable(billData.getAmountEditable());
						factura = facturaRepo.save(factura);
						response = new FacturaMultiMarketResponse(factura.getMonto(), factura.getComision(), factura.getIdReferencia(), factura.getBarcode(), factura.getReference(), factura.getAmountEditable());
						response.setCodigo(0);
						response.setMensaje("Consulta Exitosa");
					}else {
						LOG.error("Referencia no válida");
						String mensajeError = request.getBarcode()==true?"un código de barras válido":"una referencia válida";
						throw new Exception("Introducir "+mensajeError);
					}
					
				} else {
					LOG.error("El servicio no está disponible en MultiMarket: {}",convenio.getNombre());
					throw new Exception("El servicio no está disponible en MultiMarket: "+convenio.getNombre());
				}
			}else {
				throw new Exception("El servicio con id "+request.getIdConvenio()+" no existe en la base de datos");
			}
		} catch (Exception e) {
			LOG.error("Error al consultar la factura en MultiMarket {}", e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}

}
