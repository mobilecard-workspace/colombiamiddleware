package com.addcel.colombia.middleware.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.request.push.PushParams;
import com.addcel.colombia.middleware.entities.dto.request.push.PushRequest;
import com.addcel.colombia.middleware.entities.dto.response.push.PushResponse;
import com.addcel.colombia.middleware.repositories.feign.PushClient;
import com.addcel.colombia.middleware.service.PushService;
import com.addcel.colombia.middleware.utils.BdUtils;

@Service
public class PushServiceImpl implements PushService {
	
	@Autowired
	private BdUtils bdUtils;
	@Autowired
	private PushClient pushClient;
	
	private static final Logger LOG = LoggerFactory.getLogger(PushServiceImpl.class);

	@Override
	public void sendPush(Map<String, String> params, String idioma, Integer idPais, Integer idApp, long idUsuario, String modulo) {
		PushRequest request = null;
		List<PushParams> pushParams = new ArrayList<PushParams>();
		LOG.info("Preparando la peticion para enviar la notificacion push al usuario: {}", idUsuario);
		try {
			params.forEach((k,v) -> {
				LOG.info("KEY: {}, VALUE: {}", k,v);
				PushParams param = new PushParams(k, v);
				pushParams.add(param);
			});
			request = new PushRequest(bdUtils.pushNotification(modulo, idioma), idUsuario, "USUARIO", idioma, idPais, idApp, pushParams);
			LOG.info("Push Request {}",request);
			PushResponse response = pushClient.sendPushNotification(request);
			LOG.info("Push Response {}",response);
		} catch (Exception e) {
			LOG.error("Error al enviar la push: {}",e.getMessage());
		}
	}

}
