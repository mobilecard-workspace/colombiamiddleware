package com.addcel.colombia.middleware.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.ConsultaSaldoResponse;
import com.addcel.colombia.middleware.entities.dto.request.BaseTransaccionRequest;
import com.addcel.colombia.middleware.entities.dto.request.OperadoresRecargaRequest;
import com.addcel.colombia.middleware.entities.dto.request.TransaccionRequest;
import com.addcel.colombia.middleware.entities.dto.response.BaseSaldoResponse;
import com.addcel.colombia.middleware.entities.dto.response.BaseTransaccionResponse;
import com.addcel.colombia.middleware.entities.dto.response.EstadoTransaccionResponse;
import com.addcel.colombia.middleware.entities.model.ConfigPuntoRed;
import com.addcel.colombia.middleware.repositories.feign.PuntoRedClient;
import com.addcel.colombia.middleware.service.ConsultasService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@Service
public class ConsultasServiceImpl implements ConsultasService {
	
	private final static Logger LOG = LoggerFactory.getLogger(ConsultasServiceImpl.class);
	
	@Autowired
	private PuntoRedClient cliente;
	@Autowired
	private BdUtils bdUtils;

	@Override
	public ConsultaSaldoResponse consultarSaldoPuntoVenta(long idUsuario, long idApp, Integer idPais, String idioma) throws Exception{
		BaseSaldoResponse responseService = null;
		ConsultaSaldoResponse response = null;
		ConfigPuntoRed config = null;
		OperadoresRecargaRequest request = new OperadoresRecargaRequest();
		try {
			LOG.info("Inicializa el método de consulta de saldo");
			LOG.info("validando el establecimiento");
			if (bdUtils.validarUsuario(idUsuario)) {
				LOG.info("Obteniendo las credenciales de la BD");
				config = bdUtils.generateRequestPuntoRed();
				request.setClaveHost(config.getClaveHost());
				request.setProceso(AppUtils.CODPOCESO_CONSULTA_SALDO);
				request.setComercio(config.getComercio());
				request.setPuntoVenta(config.getPuntoVenta());
				request.setUsuarioHost(config.getUsuarioHost());
				LOG.info("Enviando la peticion a Punto Red: "+request);
				responseService = cliente.consultaDeSaldo(request);
				LOG.info("Respuesta de Punto Red "+responseService);
				response = new ConsultaSaldoResponse();
				response.setCodigo(0);
				response.setMensaje("Saldo consultado con exito");
				response.setSaldo(new Double(responseService.getDatos().getSaldo()));
				bdUtils.ingresarBitacoraPuntoRed("Consulta de Saldo", "Consulta exitosa", idUsuario, "COMERCIO", "Consultas Punto Red", idPais, idApp, idioma, 210L, 2100L);
			}
		} catch (Exception e) {
			LOG.error("Error al consultar el saldo del establecimiento: "+e.getCause());
			bdUtils.ingresarBitacoraPuntoRed("Consulta de Saldo", "Error: "+e.getMessage(), idUsuario, "COMERCIO", "Consultas Punto Red", idPais, idApp, idioma, 210L, 2100L);
			throw new Exception("Error al consultar el saldo, mensaje: "+e.getMessage());
		}
		return response;
	}

	@Override
	public EstadoTransaccionResponse consultaStatusTransaccion(long idUsuario, String trace, long idApp, Integer idPais, String idioma) throws Exception {
		EstadoTransaccionResponse response = null;
		ConfigPuntoRed config = null;
		BaseTransaccionResponse responseService = null;
		BaseTransaccionRequest request = new BaseTransaccionRequest();
		try {
			LOG.info("Inicializa el método de consulta del estado de la transaccion");
			LOG.info("Validando el establecimiento");
			if(bdUtils.validarUsuario(idUsuario)) {
				LOG.info("Obteniendo las credenciales de la BD");
				config = bdUtils.generateRequestPuntoRed();
				request.setClaveHost(config.getClaveHost());
				request.setUsuarioHost(config.getUsuarioHost());
				request.setComercio(config.getComercio());
				request.setProceso(AppUtils.CODPROCESO_ESTADO_TRANSACCION);
				request.setPuntoVenta(config.getPuntoVenta());
				request.setDatos(new TransaccionRequest(trace));
				LOG.info("Enviando la peticion a Punto Red: "+request);
				responseService = cliente.consultaEstadoTransaccion(request);
				LOG.info("Respuesta de punto red "+responseService);
				response = responseService.getDatos();
				bdUtils.ingresarBitacoraPuntoRed("Consulta estado de la transaccion", "Consulta exitosa", idUsuario, "COMERCIO", "Consultas Punto Red", idPais, idApp, idioma, 210L, 2100L);
			}
		} catch (Exception e) {
			LOG.error("Error al consultar el estatus de la transaccion: "+e.getCause());
			bdUtils.ingresarBitacoraPuntoRed("Consulta estado de la transaccion", "Error: "+e.getMessage(), idUsuario, "COMERCIO", "Consultas Punto Red", idPais, idApp, idioma, 210L, 2100L);
			throw new Exception("Error al consultar el estatus de la transaccion, mensaje: "+e.getMessage());
		}
		return response;
	}

}
