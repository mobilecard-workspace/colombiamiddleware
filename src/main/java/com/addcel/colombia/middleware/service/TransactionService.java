package com.addcel.colombia.middleware.service;

import com.addcel.colombia.middleware.entities.dto.RequestPaqueteService;
import com.addcel.colombia.middleware.entities.dto.RequestRecargaService;
import com.addcel.colombia.middleware.entities.dto.TransactionResponse;

public interface TransactionService {

	TransactionResponse recarga(RequestRecargaService request) throws Exception;
	TransactionResponse comprarPaquete(RequestPaqueteService request) throws Exception; 
}
