package com.addcel.colombia.middleware.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.dto.ResponseMc;
import com.addcel.colombia.middleware.entities.dto.request.BaseCategoriasRequest;
import com.addcel.colombia.middleware.entities.dto.request.BasePaquetesRequest;
import com.addcel.colombia.middleware.entities.dto.request.CategoriasRequest;
import com.addcel.colombia.middleware.entities.dto.request.OperadoresRecargaRequest;
import com.addcel.colombia.middleware.entities.dto.request.PaquetesRequest;
import com.addcel.colombia.middleware.entities.dto.response.BaseCategoriasResponse;
import com.addcel.colombia.middleware.entities.dto.response.BaseConsultaPaquetesResponse;
import com.addcel.colombia.middleware.entities.dto.response.BaseOperadoresResponse;
import com.addcel.colombia.middleware.entities.dto.response.CategoriasResponse;
import com.addcel.colombia.middleware.entities.dto.response.ConsultaPaquetesResponse;
import com.addcel.colombia.middleware.entities.dto.response.OperadoresResponse;
import com.addcel.colombia.middleware.entities.model.Categorias;
import com.addcel.colombia.middleware.entities.model.ConfigPuntoRed;
import com.addcel.colombia.middleware.entities.model.OperadoresDePaquetes;
import com.addcel.colombia.middleware.entities.model.OperadoresDeRecarga;
import com.addcel.colombia.middleware.entities.model.Paquetes;
import com.addcel.colombia.middleware.repositories.CategoriasRepository;
import com.addcel.colombia.middleware.repositories.OperadoresDePaquetesRepository;
import com.addcel.colombia.middleware.repositories.OperadoresDeRecargaRepository;
import com.addcel.colombia.middleware.repositories.PaquetesRepository;
import com.addcel.colombia.middleware.repositories.feign.PuntoRedClient;
import com.addcel.colombia.middleware.service.CatalogosService;
import com.addcel.colombia.middleware.utils.AppUtils;
import com.addcel.colombia.middleware.utils.BdUtils;

@Service
public class CatalogosServiceImpl implements CatalogosService {
	
	private static final Logger LOG = LoggerFactory.getLogger(CatalogosServiceImpl.class);
	
	@Autowired
	private CategoriasRepository categoriasRepo;
	@Autowired
	private OperadoresDeRecargaRepository operadoresRecargaRepo;
	@Autowired
	private OperadoresDePaquetesRepository operadoresPaquetesRepo;
	@Autowired
	private PaquetesRepository paquetesRepo;
	@Autowired
	private BdUtils bdUtils;
	
	//Cliente de puntored
	@Autowired
	private PuntoRedClient cliente;

	@Override
	public ResponseMc actualizarCatalogoPaquetes(Long idApp, Integer idPais, String idioma, String categoria) throws Exception{
		ResponseMc response = null;
		BaseConsultaPaquetesResponse responsePaquetes = null;
		ConfigPuntoRed configPr = null;
		BasePaquetesRequest requestPr = null;
		try {
			LOG.info("Iniciando la actualización del catalogo de paquetes");
			configPr = bdUtils.generateRequestPuntoRed();
			requestPr = new BasePaquetesRequest();
			requestPr.setClaveHost(configPr.getClaveHost());
			requestPr.setComercio(configPr.getComercio());
			requestPr.setProceso(AppUtils.CODPROCESO_CONSULTA_PAQUETES_POR_CATEGORIA);
			requestPr.setPuntoVenta(configPr.getPuntoVenta());
			requestPr.setUsuarioHost(configPr.getUsuarioHost());
			requestPr.setDatos(new PaquetesRequest(categoria));
			LOG.info("El request se armo correctamente; consultando catalogo en la Base de Datos");
			List<Paquetes> paquetesBD = paquetesRepo.findByCategoria_CodigoCategoria(categoria);
			LOG.info("Paquetes existentes en BD "+paquetesBD);
			LOG.info("Consultando el catalogo de paquetes en Punto Red");
			responsePaquetes = cliente.consultaPaquetesPorCategoria(requestPr);
			List<ConsultaPaquetesResponse> paquetsServicio = responsePaquetes.getDatos();
			LOG.info("Paquetes en Punto Red: "+responsePaquetes.getDatos());
			LOG.info("Comparando si hay nuevos paquetes o hay cambios en los actuales");
			List<ConsultaPaquetesResponse> updatedPaquetes = paquetsServicio.stream().filter(o -> paquetesBD.stream().noneMatch(o2 -> o2.getCodigoPaquete().equals(o.getCodigoPaquete()) && o2.getDescripcion().equals(o.getDescripcion()))).collect(Collectors.toList());
			LOG.info("Paquetes por agregar: "+updatedPaquetes);
			Categorias categoriaBd = categoriasRepo.findByCodigoCategoria(categoria);
			updatedPaquetes.forEach(p -> {
				LOG.info("Agregando nuevo paquete: "+p.getDescripcion()+" a la base de datos");
				Paquetes paquete = new Paquetes();
				paquete.setActivo(1);
				paquete.setCategoria(categoriaBd);
				paquete.setCodigoPaquete(p.getCodigoPaquete());
				paquete.setDescripcion(p.getDescripcion());
				paquete.setSku(p.getSku());
				paquete.setValor(p.getValor());
				paquetesRepo.save(paquete);
			});
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Paquetes", "Actualizacion exitosa", 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			response = new ResponseMc(0,"Paquetes de la Categoria: "+categoria+" Actualizados");
		} catch (Exception e) {
			LOG.error("Error al actualizar el catalogo de paquetes: "+e.getCause());
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Paquetes", "Error en la actualizacion "+e.getCause(), 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			throw new Exception("Error al actualizar el catalogo de paquetes: "+e.getCause()+", mensaje: "+e.getMessage());
		}
		return response;
	}

	@Override
	public ResponseMc actualizarCatalogoOperadoresRecarga(Long idApp, Integer idPais, String idioma) throws Exception{
		ResponseMc response = null;
		ConfigPuntoRed configPr = null;
		BaseOperadoresResponse operadoresResponse = null;
		OperadoresRecargaRequest request = new OperadoresRecargaRequest();
		try {
			LOG.info("Inicializando la actualizacion del catalogo de operadores de recarga");
			configPr = bdUtils.generateRequestPuntoRed();
			request.setClaveHost(configPr.getClaveHost());
			request.setComercio(configPr.getComercio());
			request.setProceso(AppUtils.CODPROCESO_CONSULTA_OPERADORES_RECARGA);
			request.setPuntoVenta(configPr.getPuntoVenta());
			request.setUsuarioHost(configPr.getUsuarioHost());
			LOG.info("El request se armó correctamente; consultando catalogo en la base de datos");
			final List<OperadoresDeRecarga> operadoresBD = operadoresRecargaRepo.findAll();
			LOG.info("Operadores de recarga en la Base de Datos "+operadoresBD);
			operadoresResponse = cliente.getoperadoresRecargas(request);
			List<OperadoresResponse> operadoresService = operadoresResponse.getDatos();
			LOG.info("Operadores de recarga en PuntoRed: "+operadoresResponse.getDatos());
			LOG.info("Comparando si hay cambios en el catalogo");
			List<OperadoresResponse> updateOperadores = operadoresService.stream().filter(o1 -> operadoresBD.stream().noneMatch(o2 -> o2.getCodigoProveedor().equals(o1.getCodigoProveedor()) && o2.getNombreProveedor().equals(o1.getNombreProveedor()))).collect(Collectors.toList());
			LOG.info("Operadores de Recarga por actualizar: "+updateOperadores);
			updateOperadores.forEach(o -> {
				LOG.info("Agregando operador de recarga: "+o.getNombreProveedor()+" a la base de datos");
				OperadoresDeRecarga operador = new OperadoresDeRecarga();
				operador.setActivo(1);
				operador.setCodigoProveedor(o.getCodigoProveedor());
				operador.setNombreProveedor(o.getNombreProveedor());
				operadoresRecargaRepo.save(operador);
			});
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Operadores de Recargas", "Actualizacion exitosa", 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			response = new ResponseMc(0,"Catalogo Operadores de Recarga Actualizado");
		} catch (Exception e) {
			LOG.error("Error al actualizar el catalogo de operadores de recarga: "+e.getCause());
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Operadores de Recargas", "Error en la actualizacion "+e.getCause(), 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			throw new Exception("Error al actualizar el catalogo de operadores de recarga: "+e.getCause()+", mensaje: "+e.getMessage());
		}
		return response;
	}

	@Override
	public ResponseMc actualizarCatalogoCategorias(Long idApp, Integer idPais, String idioma, String codigoProveedor) throws Exception{
		ResponseMc response = null;
		ConfigPuntoRed configPr = null;
		BaseCategoriasRequest request = new BaseCategoriasRequest();
		BaseCategoriasResponse categoriasResponse = null;
		try {
			LOG.info("Inicializando la actualizacion del catalogo de operadores de categorias por operador");
			configPr = bdUtils.generateRequestPuntoRed();
			request.setClaveHost(configPr.getClaveHost());
			request.setComercio(configPr.getComercio());
			request.setProceso(AppUtils.CODPROCESO_CONSULTA_CATEGORIA_OPERADOR);
			request.setPuntoVenta(configPr.getPuntoVenta());
			request.setUsuarioHost(configPr.getUsuarioHost());
			request.setDatos(new CategoriasRequest(codigoProveedor));
			LOG.info("El request se armó correctamente; consultando catalogo en la base de datos");
			List<Categorias> categoriasBd = categoriasRepo.findByProveedor_CodigoProveedor(codigoProveedor);
			LOG.info("Categorias en la base de datos: "+categoriasBd);
			categoriasResponse = cliente.consultaCategoriasOperador(request);
			List<CategoriasResponse> categoriasService = categoriasResponse.getDatos();
			LOG.info("Categorias en Punto Red "+categoriasService);
			LOG.info("Comparando que no haya categorias nuevas o por actualizar");
			List<CategoriasResponse> categoriasUpdate = categoriasService.stream().filter(o1 -> categoriasBd.stream().noneMatch(o2 -> o2.getCodigoCategoria().equals(o1.getCodigoCategoria()) && o2.getDescripcion().equals(o1.getDescripcion()))).collect(Collectors.toList());
			LOG.info("Categorias por actualizar: "+categoriasUpdate);
			OperadoresDePaquetes operador = operadoresPaquetesRepo.findByCodigoProveedor(codigoProveedor);
			categoriasUpdate.forEach(o -> {
				LOG.info("Agregando la categoria "+o.getDescripcion()+" a la base de datos");
				Categorias categoria = new Categorias();
				categoria.setActivo(1);
				categoria.setCodigoCategoria(o.getCodigoCategoria());
				categoria.setDescripcion(o.getDescripcion());
				categoria.setProveedor(operador);
				categoriasRepo.save(categoria);
			});
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Operadores de Categorias para el operador "+codigoProveedor, "Actualizacion exitosa", 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			response = new ResponseMc(0, "Categorias del operador de paquetes con id: "+codigoProveedor+" actualizado");
		} catch (Exception e) {
			LOG.error("Error al actualizar el catalogo de categorias: "+e.getCause());
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Operadores de Categorias para el operador "+codigoProveedor, "Error en la actualizacion "+e.getCause(), 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			throw new Exception("Error al actualizar el catalogo de categorias: "+e.getCause()+", mensaje: "+e.getMessage());
		}
		return response;
	}

	@Override
	public ResponseMc actualizarCatalogoOperadoresPaquetes(Long idApp, Integer idPais, String idioma) throws Exception{
		ResponseMc response = null;
		ConfigPuntoRed configPr = null;
		BaseOperadoresResponse operadoresResponse = null;
		OperadoresRecargaRequest request = new OperadoresRecargaRequest();
		try {
			LOG.info("Inicializando la actualizacion del catalogo de operadores de recarga");
			configPr = bdUtils.generateRequestPuntoRed();
			request.setClaveHost(configPr.getClaveHost());
			request.setComercio(configPr.getComercio());
			request.setProceso(AppUtils.CODPROCESO_CONSULTA_OPERADORES_PAQUETES);
			request.setPuntoVenta(configPr.getPuntoVenta());
			request.setUsuarioHost(configPr.getUsuarioHost());
			LOG.info("El request se armó correctamente; consultando catalogo en la base de datos");
			List<OperadoresDePaquetes> operadoresBD = operadoresPaquetesRepo.findAll();
			LOG.info("Operadores de paquetes en la BD: "+operadoresBD);
			operadoresResponse = cliente.consultaOperadoresPaquetes(request);
			List<OperadoresResponse> operadoresService = operadoresResponse.getDatos();
			LOG.info("Operadores de paquetes en PuntoRed: "+operadoresService);
			LOG.info("Comparando si hay cambios en el catalogo");
			List<OperadoresResponse> updatedOperadores = operadoresService.stream().filter(o1 -> operadoresBD.stream().noneMatch(o2 -> o2.getCodigoProveedor().equals(o1.getCodigoProveedor()) && o2.getNombreProveedor().equals(o1.getNombreProveedor()))).collect(Collectors.toList());
			LOG.info("Operadores de paquetes por actualizar: "+updatedOperadores);
			updatedOperadores.forEach(o -> {
				LOG.info("Agregando el operador de paquetes "+o.getNombreProveedor()+" a la base de datos");
				OperadoresDePaquetes operador = new OperadoresDePaquetes();
				operador.setActivo(1);
				operador.setCodigoProveedor(o.getCodigoProveedor());
				operador.setNombreProveedor(o.getNombreProveedor());
				operadoresPaquetesRepo.save(operador);
			});
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Operadores de Recarga", "Actualizacion exitosa", 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			response = new ResponseMc(0,"Catalogo Operadores de Paquetes Actualizado");
		} catch (Exception e) {
			LOG.error("Error al actualizar el catalogo de operadores de paquetes: "+e.getCause());
			bdUtils.ingresarBitacoraPuntoRed("Actualizacion Catalogo de Operadores de Recarga", "Error en la actualizacion "+e.getCause(), 0L, "ADMIN", "Catalogos", idPais, idApp, idioma, 210L, 2100L);
			throw new Exception("Error al actualizar el catalogo de operadores de paquetes: "+e.getCause()+", mensaje: "+e.getMessage());
		}
		return response;
	}

}
