package com.addcel.colombia.middleware.service;

import com.addcel.colombia.middleware.entities.dto.ResponseMc;

public interface CatalogosService {

	ResponseMc actualizarCatalogoPaquetes(Long idApp, Integer idPais, String idioma, String categoria) throws Exception;
	ResponseMc actualizarCatalogoOperadoresRecarga(Long idApp, Integer idPais, String idioma) throws Exception;
	ResponseMc actualizarCatalogoCategorias(Long idApp, Integer idPais, String idioma, String codigoProveedor) throws Exception;
	ResponseMc actualizarCatalogoOperadoresPaquetes(Long idApp, Integer idPais, String idioma) throws Exception;
}
