package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestRecargaService implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idUsuario;
	private Integer idPais;
	private Long idApp;
	private String codigoProveedor;
	private Long idTarjeta;
	private String monto;
	private String imei;
	private String idioma;
	private String concepto;
	private String numeroCelular;
}
