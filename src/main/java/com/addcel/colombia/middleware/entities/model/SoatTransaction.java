package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="soat_transaction")
public class SoatTransaction implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_transaction_soat")
	private Long idTransaction;
	@Column(name = "tarifa")
	private Integer tarifa;
	@Column(name = "fecha_expedicion")
	private String fechaExpedicion;
	@Column(name = "fecha_inicio_vigencia")
	private String fechaInicioVigencia;
	@Column(name = "fecha_fin_vigencia")
	private String fechaFinVigencia;
	@Column(name = "tasa_runt")
	private Integer valorTasaRunt;
	@Column(name = "total_poliza")
	private Integer valorTotalPoliza;
	@Column(name = "total_apagar")
	private Integer valorTotalPagar;
	@Column(name = "contribucion")
	private Integer valorContribucion;
	@Column(name = "valor_prima")
	private Integer valorPrima;
	@Column(name = "placa")
	private String placa;
	@Column(name = "ano_modelo")
	private Integer anoModelo;
	@Column(name = "cc")
	private Integer cntCc;
	@Column(name = "no_chasis")
	private String noChasis;
	@Column(name = "clase_soat")
	private String claseSoat;
	@Column(name = "cod_marca_sise")
	private String codMarcaSise;
	@Column(name = "linea")
	private String linea;
	@Column(name = "cod_tipo_veh_min_trans")
	private Integer codTipoVehMinTrans;
	@Column(name = "cod_marca")
	private String codMarca;
	@Column(name = "no_motor")
	private String noMotor;
	@Column(name = "cnt_ocupantes")
	private Integer cnt_ocupantes;
	@Column(name = "cnt_toneladas")
	private Integer cnt_toneladas;
	@Column(name = "no_vin")
	private String noVin;
	@Column(name = "id_usuario")
	private Long idUsuario;
	@Column(name = "cod_destino")
	private Integer codDestino;
	@Column(name = "status")
	private Integer status;
	@Column(name = "modelo")
	private String modelo;
}
