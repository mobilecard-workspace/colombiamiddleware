package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class CatalogoMultiMarketResponse extends ResponseMc implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<MultiMarketProductoObj> productos;
}
