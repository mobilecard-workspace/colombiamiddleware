package com.addcel.colombia.middleware.entities.dto.request.viarapida;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarPasos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tagId;
	private Long idUsuario;
	private Integer tipoConsulta;
}
