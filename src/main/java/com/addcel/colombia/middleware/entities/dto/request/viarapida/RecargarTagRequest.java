package com.addcel.colombia.middleware.entities.dto.request.viarapida;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecargarTagRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tag;
	private double monto;
	private String authNumber;
	private String placa;
	private long idUsuario;
	
}
