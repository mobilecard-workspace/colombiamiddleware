package com.addcel.colombia.middleware.entities.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="factura_multiMarket")
public class FacturaMultiMarket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura")
    private Long idReferencia;
	
	@Column(name = "reference")
	private String reference;
	
	@Column(name = "monto")
	private Double monto;
	
	@Column(name = "comision")
	private Double comision;
	
	@Column(name = "hash")
	private String hash;
	
	@Column(name = "id_convenio")
	private Long idConvenio;
	
	@Column(name = "product_id")
	private Integer productId;
	
	@Column(name = "id_usuario")
	private Long idUsuario;
	
	@Column(name = "servicio")
	private String servicio;
	
	@Column(name = "barcode")
	private Boolean barcode;
	
	@Column(name = "amount_editable")
	private Boolean amountEditable;
}
