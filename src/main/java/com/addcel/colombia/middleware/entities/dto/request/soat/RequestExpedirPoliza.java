package com.addcel.colombia.middleware.entities.dto.request.soat;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestExpedirPoliza implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer tarifa;
	private String fechaExpedicion;
	private String fechaInicioVigencia;
	private String fechaFinVigencia;
	private Integer valorTasaRUNT;
	private Integer valorTotalPoliza;
	private Integer valorTotalPagar;
	private Integer valorContribucion;
	private Integer valorPrima;
	private String correo;
	private String placa;
	private String apellido;
	private String nombre;
	private String direccion;
	private String telefono;
	private Integer anoModelo;
	private Integer cntCc;
	private String noChasis;
	private String claseSoat;
	private String codMarcaSise;
	private String linea;
	private Integer codTipoVehMinTrans;
	private String codMarca;
	private String noMotor;
	private Integer cnt_ocupantes;
	private Integer cnt_toneladas;
	private String noVin;
	private String noDoc;
	private Integer codDestino;
	private Integer codDpto;
	private Integer codMunicipio;
	private String modelo;
}
