package com.addcel.colombia.middleware.entities.dto.request.viarapida;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveTagRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idUsuario;
	private Integer idPais;
	private Long idApp;
	private String idioma;
	private String tagId;
	private String placa;
	private String alias;
	private Long idTagBd;
}
