package com.addcel.colombia.middleware.entities.dto.response.soat;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Clase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer codClase;
	private String txtDesc;
	private Integer codTipoVehMinTrans;
}
