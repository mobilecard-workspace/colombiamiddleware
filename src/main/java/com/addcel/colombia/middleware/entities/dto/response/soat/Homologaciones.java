package com.addcel.colombia.middleware.entities.dto.response.soat;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Homologaciones implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codMarcaSise;
	private Integer codLineaSise;
	private Integer codDestinoSise;
	private Clase clase;
}
