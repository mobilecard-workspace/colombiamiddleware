package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class FacturaMultiMarketResponse extends ResponseMc implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Double monto;
	private Double comision;
	private Long idReferencia;
	private Boolean barcode;
	private String reference;
	private Boolean amountEditable;
}
