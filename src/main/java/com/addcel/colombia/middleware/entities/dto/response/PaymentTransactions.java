package com.addcel.colombia.middleware.entities.dto.response;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentTransactions implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String authorization;
	private Double amount;
	private Date created;
	private String error;
	private String uuid;
	private String operative;
	private String status;
}
