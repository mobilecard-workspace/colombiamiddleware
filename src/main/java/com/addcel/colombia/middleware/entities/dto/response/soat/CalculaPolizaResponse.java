package com.addcel.colombia.middleware.entities.dto.response.soat;

import java.io.Serializable;

import com.addcel.colombia.middleware.entities.dto.ResponseMc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class CalculaPolizaResponse extends ResponseMc implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer tarifa;
	private String fechaExpedicion;
	private String fechaInicioVigencia;
	private String fechaFinVigencia;
	private Integer valorPrima;
	private Integer valorContribucion;
	private Integer valorTasaRUNT;
	private Integer valorDescuento;
	private Integer ValorTotalPoliza;
	private Integer valorTotalPagar;
	private String resultadoExito;
	private Integer diasVigencia;
	private Integer codEstadoFecFasecolda;

}
