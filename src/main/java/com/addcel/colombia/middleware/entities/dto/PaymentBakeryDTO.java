package com.addcel.colombia.middleware.entities.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentBakeryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idUsuario; 
	private Long idTarjeta; 
	private String monto; 
	private String imei;
	private String concepto; 
	private Long idApp; 
	private Integer idPais; 
	private String idioma; 
	private Long idProducto; 
	private Long idProveedor;
	private String comision;
}
