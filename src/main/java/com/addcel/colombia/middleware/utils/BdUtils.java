package com.addcel.colombia.middleware.utils;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.colombia.middleware.entities.model.BitacoraPuntoRed;
import com.addcel.colombia.middleware.entities.model.ConfigPuntoRed;
import com.addcel.colombia.middleware.entities.model.Paises;
import com.addcel.colombia.middleware.entities.model.Paquetes;
import com.addcel.colombia.middleware.entities.model.SoatTransaction;
import com.addcel.colombia.middleware.entities.model.Usuario;
import com.addcel.colombia.middleware.repositories.BitacoraPrRepository;
import com.addcel.colombia.middleware.repositories.ConfigPuntoRedRepository;
import com.addcel.colombia.middleware.repositories.OperadoresDeRecargaRepository;
import com.addcel.colombia.middleware.repositories.PaisesRepository;
import com.addcel.colombia.middleware.repositories.PaquetesRepository;
import com.addcel.colombia.middleware.repositories.PushRepository;
import com.addcel.colombia.middleware.repositories.SoatTransactionRepository;
import com.addcel.colombia.middleware.repositories.TablaParametricaRepository;
import com.addcel.colombia.middleware.repositories.TarjetaCreditoRepository;
import com.addcel.colombia.middleware.repositories.UsuarioRepository;

@Service
public class BdUtils {
	
	private final static Logger LOG = LoggerFactory.getLogger(BdUtils.class);

	@Autowired
	private ConfigPuntoRedRepository configRepo;
	@Autowired
	private UsuarioRepository usuarioRepo;
	@Autowired
	private BitacoraPrRepository bitacora;
	@Autowired
	private TarjetaCreditoRepository tarjetarepository;
	@Autowired
	private PaisesRepository paisRepo;
	@Autowired
	private PaquetesRepository paquetesRepo;
	@Autowired
	private SoatTransactionRepository soatRepo;
	@Autowired
	private OperadoresDeRecargaRepository operadoresRecaragaRepo;
	@Autowired
	private PushRepository pushRepo;
	@Autowired
	private TablaParametricaRepository tablaParametricaRepo;
	
	public boolean validarUsuario(Long idUsuario) throws Exception {
		try {
			LOG.info("Inicia validacion del usuario con id: {}",idUsuario);
			return usuarioRepo.existsById(idUsuario);
		} catch (Exception e) {
			LOG.error("Error al validar el usuario en la BD: {}",e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	public ConfigPuntoRed generateRequestPuntoRed() throws Exception{
		try {
			LOG.info("Obteniendo las credenciales de punto red");
			return configRepo.findById(1).get();
		} catch (Exception e) {
			LOG.error("Error al recuperar las credenciales de punto red {}",e.getMessage());
			throw new Exception("Error al recuperar las credenciales de punto red "+e.getMessage());
		} 
	}
	
	public void ingresarBitacoraPuntoRed(String accion, String resultado, Long idUsuario, String tipoUsuario, String servicio, Integer idPais, Long idApp, String  idioma, Long idProducto, Long idProveedor) {
		BitacoraPuntoRed bit = new BitacoraPuntoRed();
		try {
			LOG.info("Insertando en la bitacora de Colombia Middleware la accion: "+accion);
			bit.setAccion(accion);
			bit.setFecha(new Date());
			bit.setIdUsuario(idUsuario);
			bit.setResultado(resultado);
			bit.setServicio(servicio);
			bit.setIdApp(idApp);
			bit.setIdPais(idPais);
			bit.setIdioma(idioma);
			bit.setIdProducto(idProducto);
			bit.setIdProveedor(idProveedor);
			bitacora.save(bit);
			LOG.info("Insertado con exito en la bitacora");
		} catch (Exception e) {
			LOG.error("Error al insertar la bitacora: "+e.getCause()+", mensaje: "+e.getMessage());
		}
	}
	
	public Usuario consultarUsuario(long idUsuario) throws Exception{
		try {
			LOG.info("Inicia validacion del usuario con id: {}",idUsuario);
			return usuarioRepo.findById(idUsuario).orElse(null);
		} catch (Exception e) {
			LOG.error("Error al consultar el usuario en la Base de Datos con el id: {}; {}",idUsuario,e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	public Boolean obtenerDatosTarjeta(long idTarjeta) throws Exception{
		try {
			LOG.info("Inicia consulta de la tarjeta con el id: {}",idTarjeta);
			return tarjetarepository.existsById(idTarjeta);
		} catch (Exception e) {
			LOG.error("Error al consultar la tarjeta en la Base de Datos con el id: {}; {}",idTarjeta,e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	public Boolean validaPais(Integer idPais) throws Exception {
		Paises colombia = null;
		boolean paisValid = false;
		try {
			colombia = paisRepo.findById(idPais).orElse(null);
			if (colombia != null && colombia.getNombre() == "COLOMBIA") {
				paisValid = true;
			}
		} catch (Exception e) {
			LOG.info("Error al validar el pais: "+e.getMessage());
			throw new Exception("Error al validar el pais");
		}
		return paisValid;
	}
	
	public Paquetes consultaPaquete(Integer idPaquete) throws Exception {
		try {
			LOG.info("Consulta del paquete con id: "+idPaquete);
			return paquetesRepo.findById(idPaquete).orElse(null);
		} catch (Exception e) {
			LOG.info("Error al buscar el paquete en BD "+e);
			throw new Exception("Error al buscar el paquete en BD");
		}
	}
	
	public SoatTransaction consultarPolizaBd(String placa, Long idUsuario, Integer status) throws Exception {
		try {
			LOG.info("Recuperando los datos de la poliza para la placa {}, y el usuario {}",placa,idUsuario);
			return soatRepo.findByPlacaAndIdUsuarioAndStatus(placa, idUsuario, status);
		} catch (Exception e) {
			LOG.error("Error al obtener los datos de la poliza "+e);
			throw new Exception("Error al obtener los datos de la poliza");
		}
	}
	
	public String nombreOperadorRecarga(String codigoProveedor) {
		return operadoresRecaragaRepo.findNombreproveedor(codigoProveedor);
	}
	
	public Long pushNotification(String modulo, String idioma) {
		return pushRepo.findByModuloAndIdioma(modulo, idioma);
	}
	
	public Integer validaOrganismo(String organismo) {
		LOG.info("Validando el organismo de transito {}",organismo);
		return tablaParametricaRepo.validateOrganismo(organismo, "ACTIVO");
	}
}
